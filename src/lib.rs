//! Library for managing the synchronization of keys between multiple (presumably, in the form of
//! backups) GNUPG home directories and folders, declaratively. In future, it may be possible to do this for
//! non-gnupg certificate management systems. Given that GnuPG in particular has a fair number of
//! issues and some questionable vulnerabilities, this is likely to be desirable in future.
//! Unfortunately, I don't know of any other relatively user friendly system with the same degree
//! of PKI, and I still use it.
//!
//! This program should also work with fairly minimal modifications for anything postquantum. None
//! of it is really dependent on key algorithms. However, it currently uses the GPGME library as
//! the primary mechanism for resolving things like key revocations, adding new keys, etc. and
//! provides no mechanisms of it's own for doing so. 
//!
//! # KEY-IDENTITIES
//! Each *identity* is (conceptually) composed of the following asymmetric-crypto components:
//! * A *primary key* - This is used to sign subkeys and indicate trust in subkeys
//! * Various *subkeys* with distinct capabilities - This is what is used to encrypt and decrypt
//!   day-to-day. 
//!
//! When specifying identities, you need to do the following:
//! * Provide an *identity name*
//! * Provide a *primary key identifier* (in particular, this is likely to be your keygrip or key
//!   hash for gpg)
//! * Optionally, provide identifiers to subkeys - for example, you might call one subkey `ssh`,
//!   which would indicate that any place which specifically needs the "ssh" keys should use this. 
//! 
//! # KEY-DATABASES
//! Key-Databases fundamentally represent the places where keys should be (1) retrieved from and
//! (2) placed back into. Synchronising key-databases is the primary purpose of this program (and
//! this library provides most of the tools to do so). 
//!
//! Currently, key-databases correspond to one of two things:
//! * GnuPG homedirs - From which keys are both read and written to.
//! * Key Directories - Generally, keys are just written to these - used for, for example, creating
//!   simple key files you can give to your friends, or producing SSH keys you can upload to
//!   git{hub/lab/ea} or some other server.
//!
//! # LIBRARY ARCHITECTURE
//! This library actually provides a fairly sophisticated architecture to help with meaningful key
//! location and translation between different formats (for example, when emitting OpenSSH keys
//! from a GnuPG subkey).
//!
//! ## High-Level Key Data Unification Algorithm
//! When using this library, it's important to understand the general architecture that it's
//! designed to enable, and the full extent of it's purpose. 
//!
//! The purpose of the library is to *collect* all information about identities from various sources, 
//! *merge* that information (using some backend capable of resolving certificates including, for instance, 
//! revokations), then *re-emit* the up-to-date information (or some parts of it) back into all the
//! mentioned databases. 
//!
//! Each database should, at the end of this process, *only* contain information that it was
//! specified to have. For instance, if a database is not allowed to have the primary private key
//! of a certificate, then even if the unified information contains such a primary private key it
//! should not be emitted back to that database (and in fact, it may be desireable to delete any
//! excess information).
//!
//! Furthermore, some databases may be lossy. For instance, OpenSSH keys will not preserve the
//! subkey/primary key relations of GnuPG certificates.
//!
//! ## Memory & Files
//! Things get a little complex when we have to worry about the fact that sometimes import and
//! export may only be allowed to produce files, whereas other times they always produce an
//! in-memory peice of data.
//!
//! Furthermore, there are various degrees of convertability and lossiness between different
//! formats - for example, exporting SSH keys loses a lot of information, whereas exporting GPG
//! keys generally bundles information. 
//!
//! To manage this, we define *import* and *export* capabilities. Each backend indicates how much
//! information it can gain from importing a specific key type (and how much it can merge
//! information with it's existing db), as well as how much structural information will remain from
//! a certificate if exported directly.
//!
//! This is usually done directly into memory, but hopefully as raw bytes into standardized cert
//! formats of some kind.
//!
//! ## Export Tree 
pub mod config;
pub mod backends;
pub mod certs;
pub mod keydb;


// gpg-keysync
// Copyright (C) 2023 Matti
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
