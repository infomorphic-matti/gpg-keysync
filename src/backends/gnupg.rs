use crate::certs;

use super::{Backend, ImportCertResolutionCapability};

pub mod ctx;

pub struct GnuPG {
    pub(crate) lib: gpgme::Gpgme,
}

impl From<gpgme::Gpgme> for GnuPG {
    fn from(value: gpgme::Gpgme) -> Self {
        Self { lib: value }
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Hash)]
pub struct UserId {
    pub specifier: String 
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Hash)]
pub struct KeyFingerprint {
    pub key_fingerprint: String,
    pub guess_best_actual_key: bool
}


#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Hash)]
/// Can only be used directly for subkeys I think? 
pub struct Keygrip(pub String);

impl Backend for GnuPG {
    type DbContext = ctx::GnuPGHomeDirectory;

    type CertIdentifier = UserId;

    type PrimaryKeyIdentifier = KeyFingerprint;

    type SubKeyIdentifier = Keygrip;

    fn import_cert_behaviour_from_memory_for(
        &self,
        certificate_ty: certs::CertType,
    ) -> std::option::Option<super::ImportCertResolutionCapabilities> {
        Some(match certificate_ty {
            certs::CertType::GPG => super::ImportCertResolutionCapabilities {
                primary_public_key: ImportCertResolutionCapability::TotalMerge,
                primary_secret_key: ImportCertResolutionCapability::TotalMerge,
                sub_public_key: ImportCertResolutionCapability::TotalMerge,
                sub_secret_key: ImportCertResolutionCapability::TotalMerge,
            },
            certs::CertType::OpenSSH => super::ImportCertResolutionCapabilities {
                primary_public_key: ImportCertResolutionCapability::None,
                primary_secret_key: ImportCertResolutionCapability::None,
                sub_public_key: ImportCertResolutionCapability::None,
                sub_secret_key: ImportCertResolutionCapability::None,
            },
        })
    }

    fn export_cert_behaviour_to_memory_for(
        &self,
        certificate_ty: certs::CertType,
    ) -> std::option::Option<super::ExportCertPreservationBehaviour> {
        Some(match certificate_ty {
            certs::CertType::GPG => super::ExportCertPreservationBehaviour::Full,
            certs::CertType::OpenSSH => super::ExportCertPreservationBehaviour::None,
        })
    }
}

// gpg-keysync
// Copyright (C) 2023 Matti
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
