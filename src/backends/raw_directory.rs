//! Backend indicating raw directories that just have files of *some name* containing keys.

use std::path::PathBuf;

use crate::certs;

use super::Backend;

pub struct CertFile(pub String);
pub struct PrimaryKeyFile(pub String);
pub struct SubKeyFile(pub String);

/// Backend for simple raw directories.
pub struct RawDirBackend;

impl Backend for RawDirBackend {
    type DbContext = PathBuf;

    type CertIdentifier = CertFile;

    type PrimaryKeyIdentifier = PrimaryKeyFile;

    type SubKeyIdentifier = SubKeyFile;

    fn import_cert_behaviour_from_memory_for(
        &self,
        _certificate_ty: certs::CertType,
    ) -> Option<super::ImportCertResolutionCapabilities> {
        // We can never merge certificates - after all, this just dumps certs to a file directly
        // without checking other contents :)
        Some(super::ImportCertResolutionCapabilities {
            primary_public_key: super::ImportCertResolutionCapability::None,
            primary_secret_key: super::ImportCertResolutionCapability::None,
            sub_public_key: super::ImportCertResolutionCapability::None,
            sub_secret_key: super::ImportCertResolutionCapability::None,
        })
    }

    fn export_cert_behaviour_to_memory_for(
        &self,
        certificate_ty: certs::CertType,
    ) -> Option<super::ExportCertPreservationBehaviour> {
        // In this case, preservation just matches whether the raw certificate type can preserve
        // hierarchy data, as that indicates if a file of that type can.
        let meta = certificate_ty.metadata();
        Some(
            match meta.contains_cert_hierarchy_metadata && meta.contains_publicity_information {
                true => super::ExportCertPreservationBehaviour::Full,
                false => super::ExportCertPreservationBehaviour::None,
            },
        )
    }
}

// gpg-keysync
// Copyright (C) 2023 Matti
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
