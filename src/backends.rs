//! Module for different backends and their capabilities.

use crate::certs;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum ImportCertResolutionCapability {
    /// Backend cannot merge this type of imported key component with other key components of the
    /// same identity it might know about
    None, 
    /// This key can unify information from this type of imported key component with information
    /// it already has about the same key component, but not if it only has information on deeper 
    /// (sub-components) or shallower (primary key) components. i.e. it can't identify the parents
    /// or children of an imported key automatically.
    MergeSameLevel,
    /// This backend can unify information from this key component with any child key components it
    /// knows about, but it can't identify if this key component is itself a child of another (even
    /// if it can't identify the parent) unless it already knows about the key's existence
    /// directly.
    MergeSameAndDeeperLevel,
    TotalMerge
} 

/// How a backend acts when it imports keys from a given type of certificate data. 
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ImportCertResolutionCapabilities {
    primary_public_key: ImportCertResolutionCapability,
    primary_secret_key: ImportCertResolutionCapability,
    sub_public_key: ImportCertResolutionCapability,
    sub_secret_key: ImportCertResolutionCapability
} 

/// How much information is preserved when emitting a full certificate. 
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum ExportCertPreservationBehaviour {
    /// It always preserves the structure of the keys that have been asked to be exported (even if
    /// they are subkeys rather than the primary key
    Full,
    /// It only exports the exact specified key. 
    None,
} 

/// Backend type.
pub trait Backend {
    /// Context data for a particular backend, typically generatable from a directory path.
    ///
    /// This represents the core location of database information on-disk or in-memory, and any other
    /// context. For example, with gnupg, this likely contains the gnugp home directory.
    type DbContext;

    /// Identifier for a certificate as-a-whole.
    type CertIdentifier; 

    /// Identifier specifically for a primary key - may be public or private. May be the same as a cert identifier. 
    /// Your cert system needs to be able to resolve public key specifiers into private keys (or
    /// perform operations on the private key using the public key identifier).  
    type PrimaryKeyIdentifier;

    /// Identifier specifically for a subkey - may be public or private. Your certificate system
    /// must be able to perform actions on the corresponding private subkey by using it's public
    /// subkey's identifier. 
    type SubKeyIdentifier;

    /// Behaviour when importing a certificate of the given type.
    ///
    /// Return [`None`] to indicate that you can't
    fn import_cert_behaviour_from_memory_for(&self, certificate_ty: certs::CertType) -> Option<ImportCertResolutionCapabilities>;

    /// Behaviour when exporting a certificate as the given type (even when potentially told to
    /// only include some parts of the keys). Return [`None`] to indicate you can't export parts of
    /// (or entire) certificates as the given type. This is assuming your database has a mechanism
    /// to correctly retrieve the certificate. 
    fn export_cert_behaviour_to_memory_for(&self, certificate_ty: certs::CertType) -> Option<ExportCertPreservationBehaviour>;
}



pub mod gnupg;
pub mod raw_directory;

// gpg-keysync
// Copyright (C) 2023 Matti
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
