//! Module for different types of certificates. For example, gpg exported key files, openssh keys,
//! OpenSSL certs.


#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Deserialize, serde::Serialize)]
pub enum CertType {
    GPG,
    OpenSSH,
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug, Hash)]
pub struct CertMetadata {
    /// If true, this certificate contains, for example, enough information to determine if it's a
    /// subkey (or collection of them), or a primary key, or similar.
    ///
    /// If false, this doesn't actually contain that information. This may be the case for raw keys
    /// or ssh keys or others.
    pub contains_cert_hierarchy_metadata: bool,

    /// If key(s) inside this certificate type know whether they are public or private keys. This
    /// is probably the case, even if it isn't explicitly encoded, because public and private keys
    /// usually have different structures.
    pub contains_publicity_information: bool,
}

impl CertType {
    pub fn metadata(&self) -> CertMetadata {
        match self {
            CertType::GPG => CertMetadata {
                contains_cert_hierarchy_metadata: true,
                contains_publicity_information: true,
            },
            CertType::OpenSSH => CertMetadata {
                contains_cert_hierarchy_metadata: false,
                contains_publicity_information: true,
            },
        }
    }
}


// gpg-keysync
// Copyright (C) 2023 Matti
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
